package tn.esprit.awsdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AwsDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsDashboardApplication.class, args);
	}

}
