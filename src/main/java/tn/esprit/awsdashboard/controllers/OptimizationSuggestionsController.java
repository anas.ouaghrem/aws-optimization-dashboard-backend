package tn.esprit.awsdashboard.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import tn.esprit.awsdashboard.entities.*;
import tn.esprit.awsdashboard.services.Ec2SuggestionsService;
import tn.esprit.awsdashboard.services.RdsSuggestionsService;
import tn.esprit.awsdashboard.services.S3SuggestionsService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
public class OptimizationSuggestionsController {

    // Local Variables
    private final Ec2SuggestionsService ec2SuggestionsService;
    private final S3SuggestionsService s3SuggestionsService;
    private final RdsSuggestionsService rdsSuggestionsService;

    // Endpoints

    //Get Methods
    @GetMapping("/suggestions/ec2")
    public List<EC2OptimizationSuggestion> getEc2Suggestions(){
        return ec2SuggestionsService.getAllEc2Suggestions();
    }

    @GetMapping("/suggestions/ebs")
    public List<EBSOptimizationSuggestion> getEbsSuggestions(){
        return ec2SuggestionsService.getAllEbsSuggestions();
    }

    @GetMapping("/suggestions/eip")
    public List<EIPOptimizationSuggestion> getEipSuggestions(){
        return ec2SuggestionsService.getAllEipSuggestions();
    }

    @GetMapping("/suggestions/s3")
    public List<S3OptimizationSuggestion> getS3Suggestions() throws Exception {
        return s3SuggestionsService.getAllS3Suggestions();
    }

    @GetMapping("/suggestions/rds")
    public List<RDSOptimizationSuggestion> getRdsSuggestions() throws Exception {
        return rdsSuggestionsService.getAllRdsSuggestions();
    }

    //Post Methods

    @PostMapping("/suggestions/ec2")
    public List<EC2OptimizationSuggestion> generateEc2Suggestions() throws Exception {
        return ec2SuggestionsService.generateEc2Suggestions();
    }

    @PostMapping("/suggestions/ebs")
    public List<EBSOptimizationSuggestion> generateEbsSuggestions() throws Exception {
        return ec2SuggestionsService.generateEbsSuggestions();
    }

    @PostMapping("/suggestions/eip")
    public List<EIPOptimizationSuggestion> generateEipSuggestions() throws Exception {
        return ec2SuggestionsService.generateEipSuggestions();
    }

    @PostMapping("/suggestions/s3")
    public List<S3OptimizationSuggestion> generateS3Suggestions() throws Exception {
        return s3SuggestionsService.generateS3Suggestions();
    }

    @PostMapping("/suggestions/rds")
    public List<RDSOptimizationSuggestion> generateRdsSuggestions() throws Exception {
        return rdsSuggestionsService.generateRdsSuggestions();
    }
}
