package tn.esprit.awsdashboard.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.esprit.awsdashboard.entities.EBSVolume;
import tn.esprit.awsdashboard.entities.EC2Instance;
import tn.esprit.awsdashboard.entities.EIPAddress;
import tn.esprit.awsdashboard.services.Ec2Service;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
public class Ec2Controller {

    private final Ec2Service ec2Service;

    @GetMapping("/getEc2")
    public List<EC2Instance> getEc2Instances(){
        return ec2Service.getAllInstances();
    }

    @GetMapping("/getEbs")
    public List<EBSVolume> getEbsVolumes(){
        return ec2Service.getAllVolumes();
    }

    @GetMapping("/getEip")
    public List<EIPAddress> getEipAddresses(){
        return ec2Service.getAllAddresses();
    }
}
