package tn.esprit.awsdashboard.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.esprit.awsdashboard.DTO.RdsDTO;
import tn.esprit.awsdashboard.entities.RDSInstance;
import tn.esprit.awsdashboard.services.RdsService;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class RDSController {

    private final RdsService rdsService;

    @GetMapping("/getRDS")
    public List<RDSInstance> getRDSInstances(){
        return rdsService.getAllRdsInstances();
    }


}
