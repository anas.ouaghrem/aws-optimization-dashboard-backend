package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.S3OptimizationSuggestion;

@Repository
public interface S3OptimizationSuggestionRepository extends JpaRepository<S3OptimizationSuggestion,Long> {
}
