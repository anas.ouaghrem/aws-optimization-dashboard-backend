package tn.esprit.awsdashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EIPAddress;

@Repository
public interface EIPAddressRepository extends JpaRepository<EIPAddress,Long> {
}
