package tn.esprit.awsdashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.S3ObjectOptimizationSuggestion;

@Repository
public interface S3ObjectOptimizationSuggestionRepository extends JpaRepository<S3ObjectOptimizationSuggestion, Long> {
}
