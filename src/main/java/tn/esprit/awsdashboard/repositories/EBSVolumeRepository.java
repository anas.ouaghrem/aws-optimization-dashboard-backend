package tn.esprit.awsdashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EBSVolume;

@Repository
public interface EBSVolumeRepository extends JpaRepository<EBSVolume,Long> {
}
