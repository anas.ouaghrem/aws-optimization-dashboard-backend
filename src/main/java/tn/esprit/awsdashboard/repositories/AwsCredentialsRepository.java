package tn.esprit.awsdashboard.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import tn.esprit.awsdashboard.entities.AwsAccountCredentials;

@Repository
public interface AwsCredentialsRepository extends JpaRepository<AwsAccountCredentials,Long> {
    AwsAccountCredentials findAwsAccountCredentialsByAccountName(String accountName);
    AwsAccountCredentials findAwsAccountCredentialsByAccessKeyId(String accessKeyId);

    AwsAccountCredentials findAwsAccountCredentialsByAccountId(String accountId);
}
