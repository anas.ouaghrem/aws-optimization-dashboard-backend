package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EBSOptimizationSuggestion;

@Repository
public interface EbsOptimizationSuggestionRepository extends JpaRepository<EBSOptimizationSuggestion,Long> {
}
