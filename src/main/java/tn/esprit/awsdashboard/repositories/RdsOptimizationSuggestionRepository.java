package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.RDSOptimizationSuggestion;

@Repository
public interface RdsOptimizationSuggestionRepository extends JpaRepository<RDSOptimizationSuggestion,Long> {
}
