package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EC2Instance;

@Repository
public interface EC2InstanceRepository extends JpaRepository<EC2Instance,Long> {
}
