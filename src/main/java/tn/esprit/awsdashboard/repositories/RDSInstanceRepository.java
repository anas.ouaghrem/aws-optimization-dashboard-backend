package tn.esprit.awsdashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.RDSInstance;

@Repository
public interface RDSInstanceRepository extends JpaRepository<RDSInstance, Long> {
}
