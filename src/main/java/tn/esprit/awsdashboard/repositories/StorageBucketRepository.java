package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.StorageBucket;

@Repository
public interface StorageBucketRepository extends JpaRepository<StorageBucket,Long> {

    public StorageBucket findByName(String name);
}
