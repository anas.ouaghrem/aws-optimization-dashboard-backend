package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EC2OptimizationSuggestion;

@Repository
public interface Ec2OptimizationSuggestionRepository extends JpaRepository<EC2OptimizationSuggestion,Long> {
}
