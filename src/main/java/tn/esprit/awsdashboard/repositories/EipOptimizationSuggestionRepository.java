package tn.esprit.awsdashboard.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.awsdashboard.entities.EIPOptimizationSuggestion;

@Repository
public interface EipOptimizationSuggestionRepository extends JpaRepository<EIPOptimizationSuggestion,Long> {
}
