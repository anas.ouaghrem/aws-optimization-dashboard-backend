package tn.esprit.awsdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EIPAddress  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String associatedAccount;

    private String allocationId;
    private String associationId;
    private String publicIp;
    private String privateIp;
    private String instanceId;
    private LocalDateTime creationTimestamp;
    private LocalDateTime lastUpdateTimestamp;
    private LocalDateTime lastUsedTimestamp;
    private LocalDateTime lastAvailableTimestamp;
}
