package tn.esprit.awsdashboard.entities;

public enum SuggestionStatus {
    Pending,
    Ignored,
    InProgress,
    Done,
}
