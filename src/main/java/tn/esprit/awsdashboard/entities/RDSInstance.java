package tn.esprit.awsdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RDSInstance  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String associatedAccount;
    private String dbInstanceIdentifier;
    private String dbInstanceClass;
    private String status;
    private String engine;
    private String engineVersion;
    private String region;
    private String ownerEmail;
    private String productId;
    private String operationHours;
    private String clientName;
    private Date creationDate;
    private int allocatedStorage;
    private String endpointAddress;
    private int endpointPort;
    private LocalDateTime creationTimestamp;
    private LocalDateTime lastUpdateTimestamp;
    private LocalDateTime lastDowntimeTimestamp;
    private LocalDateTime lastUptimeTimestamp;
}
