package tn.esprit.awsdashboard.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EBSVolume {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String associatedAccount;

    private String volumeId;
    private String volumeType;
    private Date creationTime;
    private String availabilityZone;
    private long size;
    private String state;
    private String productId;
    private String ownerEmail;
    private String operationHours;
    private String environmentType;
    private String instanceId;
    private LocalDateTime creationTimestamp;
    private LocalDateTime lastUpdateTimestamp;
}
