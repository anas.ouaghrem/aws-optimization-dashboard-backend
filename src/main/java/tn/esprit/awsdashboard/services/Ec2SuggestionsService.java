package tn.esprit.awsdashboard.services;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tn.esprit.awsdashboard.entities.*;
import tn.esprit.awsdashboard.repositories.EbsOptimizationSuggestionRepository;
import tn.esprit.awsdashboard.repositories.Ec2OptimizationSuggestionRepository;
import tn.esprit.awsdashboard.repositories.EipOptimizationSuggestionRepository;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class Ec2SuggestionsService {

    // Local Variables

    private final Ec2OptimizationSuggestionRepository ec2OptimizationSuggestionRepository;
    private final EbsOptimizationSuggestionRepository ebsOptimizationSuggestionRepository;
    private final EipOptimizationSuggestionRepository eipOptimizationSuggestionRepository;
    private final Ec2Service ec2Service;
    private final AutoscalingService autoscalingService;

    // EIP Optimization Suggestions
    //@DependsOn("DataStorageService")
    //@Scheduled(fixedDelay = 3600000L)
    public List<EIPOptimizationSuggestion> generateEipSuggestions(){
        List<EIPAddress> addresses = ec2Service.getAllAddresses();
        List<EIPOptimizationSuggestion> suggestions = new ArrayList<>();
        for (EIPAddress address : addresses){
            if(address.getInstanceId() == null) {
                EIPOptimizationSuggestion suggestion = new EIPOptimizationSuggestion();
                suggestion.setTitle("EIP with allocation ID: " + address.getAllocationId() + " not associated with a running instance");
                suggestion.setDescription("Associate the EIP with an instance");
                suggestion.setStatus(SuggestionStatus.Pending);
                suggestion.setCreatedDate(new Date());
                suggestion.setAssociatedAccount(address.getAssociatedAccount());
                suggestions.add(suggestion);
            }
            if (address.getAssociationId() == null) {
                EIPOptimizationSuggestion suggestion = new EIPOptimizationSuggestion();
                suggestion.setTitle("EIP with allocation ID: " + address.getAllocationId() + " not associated with an instance");
                suggestion.setDescription("Associate the EIP with an instance");
                suggestion.setStatus(SuggestionStatus.Pending);
                suggestion.setCreatedDate(new Date());
                suggestion.setAssociatedAccount(null);
                suggestions.add(suggestion);
            }
        }
        return eipOptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EIPOptimizationSuggestion> getAllEipSuggestions(){
        return eipOptimizationSuggestionRepository.findAll();
    }

    public EIPOptimizationSuggestion getEipSuggestionById(Long id){
        return eipOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EIPOptimizationSuggestion updateEipSuggestion(SuggestionStatus status, Long id){
        EIPOptimizationSuggestion suggestion = eipOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return eipOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEipSuggestion(Long id){
        eipOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEipSuggestions(){
        List<EIPOptimizationSuggestion> suggestions = eipOptimizationSuggestionRepository.findAll();
        for (EIPOptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending && Duration.between(suggestion.getCreatedDate().toInstant(), new Date().toInstant()).toDays() > 30){
                eipOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }

    // EC2 Optimization Suggestions
   // @DependsOn("DataStorageService")
   // @Scheduled(fixedDelay = 3600000L)
    public List<EC2OptimizationSuggestion> generateEc2Suggestions(){
        List<EC2OptimizationSuggestion> suggestions = new ArrayList<>();
        List<EC2Instance> instances = ec2Service.getAllInstances();
        for (EC2Instance instance : instances){
            // Auto Scaling Group Suggestions
            if(!autoscalingService.isInstanceInAutoScalingGroup(instance.getInstanceId())){
                EC2OptimizationSuggestion suggestion = EC2OptimizationSuggestion.builder()
                        .title("EC2 Instance with ID: " + instance.getInstanceId() + " not in an Auto Scaling Group")
                        .description("Add the instance to an Auto Scaling Group")
                        .status(SuggestionStatus.Pending)
                        .createdDate(new Date())
                        .associatedAccount(instance.getAssociatedAccount())
                        .build();
                suggestions.add(suggestion);
            }
        }
        return ec2OptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EC2OptimizationSuggestion> getAllEc2Suggestions(){
        return ec2OptimizationSuggestionRepository.findAll();
    }

    public EC2OptimizationSuggestion getEc2SuggestionById(Long id){
        return ec2OptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EC2OptimizationSuggestion updateEc2Suggestion(SuggestionStatus status, Long id){
        EC2OptimizationSuggestion suggestion = ec2OptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return ec2OptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEc2Suggestion(Long id){
        ec2OptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEc2Suggestions(){
        List<EC2OptimizationSuggestion> suggestions = ec2OptimizationSuggestionRepository.findAll();
        for (EC2OptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending && Duration.between(suggestion.getCreatedDate().toInstant(), new Date().toInstant()).toDays() > 30){
                ec2OptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }


    // EBS Optimization Suggestions
    //@DependsOn("DataStorageService")
   // @Scheduled(fixedDelay = 3600000L)
    public List<EBSOptimizationSuggestion> generateEbsSuggestions(){
        List<EBSOptimizationSuggestion> suggestions = new ArrayList<>();
        List<EBSVolume> volumes = ec2Service.getAllVolumes();
        for (EBSVolume volume : volumes){
            if(volume.getState().equals("available")){
                EBSOptimizationSuggestion suggestion = new EBSOptimizationSuggestion();
                suggestion.setTitle("EBS Volume with ID: " + volume.getVolumeId() + " is available");
                suggestion.setDescription("Delete the volume");
                suggestion.setStatus(SuggestionStatus.Pending);
                suggestion.setCreatedDate(new Date());
                suggestion.setAssociatedAccount(null);
                suggestions.add(suggestion);
            }
            if(volume.getState().equals("in-use") && volume.getInstanceId() == null){
                EBSOptimizationSuggestion suggestion = new EBSOptimizationSuggestion();
                suggestion.setTitle("EBS Volume with ID: " + volume.getVolumeId() + " is in use but not attached to an instance");
                suggestion.setDescription("Attach the volume to an instance");
                suggestion.setStatus(SuggestionStatus.Pending);
                suggestion.setCreatedDate(new Date());
                suggestion.setAssociatedAccount(null);
                suggestions.add(suggestion);
            }
        }
        return ebsOptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<EBSOptimizationSuggestion> getAllEbsSuggestions(){
        return ebsOptimizationSuggestionRepository.findAll();
    }

    public EBSOptimizationSuggestion getEbsSuggestionById(Long id){
        return ebsOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public EBSOptimizationSuggestion updateEbsSuggestion(SuggestionStatus status, Long id){
        EBSOptimizationSuggestion suggestion = ebsOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return ebsOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteEbsSuggestion(Long id){
        ebsOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithEbsSuggestions(){
        List<EBSOptimizationSuggestion> suggestions = ebsOptimizationSuggestionRepository.findAll();
        for (EBSOptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending && Duration.between(suggestion.getCreatedDate().toInstant(), new Date().toInstant()).toDays() > 30){
                ebsOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }
}
