package tn.esprit.awsdashboard.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tn.esprit.awsdashboard.entities.*;
import tn.esprit.awsdashboard.repositories.S3ObjectOptimizationSuggestionRepository;
import tn.esprit.awsdashboard.repositories.S3OptimizationSuggestionRepository;
import tn.esprit.awsdashboard.repositories.StorageBucketRepository;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor

public class S3SuggestionsService {

    // Local variables

    private final S3OptimizationSuggestionRepository s3OptimizationSuggestionRepository;
    private final S3ObjectOptimizationSuggestionRepository s3ObjectOptimizationSuggestionRepository;
    private final StorageBucketRepository storageBucketRepository;
    private final CredentialsService credentialsService;
    private final S3Service s3Service;

    // Methods

    //@Scheduled(fixedDelay = 3600000L)
    public List<S3OptimizationSuggestion> generateS3Suggestions() throws Exception {

        List<StorageBucket> buckets = storageBucketRepository.findAll();
        List<S3OptimizationSuggestion> suggestions = new ArrayList<>();
        for (StorageBucket bucket : buckets) {
            if (!hasLifecyclePolicy(bucket.getName())) {
                S3OptimizationSuggestion suggestion = S3OptimizationSuggestion.builder()
                        .title("Add Lifecycle Policy to Bucket: " + bucket.getName())
                        .description("Bucket " + bucket.getName() + " does not have a lifecycle policy. Consider adding one to " +
                                "automatically transition objects to lower-cost storage classes or delete them when they " +
                                "are no longer needed.")
                        .createdDate(new Date())
                        .status(SuggestionStatus.Pending)
                        .associatedAccount(bucket.getAssociatedAccount())
                        .build();
                suggestions.add(suggestion);
            }
        }

        return s3OptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<S3OptimizationSuggestion> getAllS3Suggestions() {
        return s3OptimizationSuggestionRepository.findAll();
    }

    public S3OptimizationSuggestion getS3SuggestionById(Long id) {
        return s3OptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public S3OptimizationSuggestion updateS3SuggestionStatus(SuggestionStatus status, Long id) {
        S3OptimizationSuggestion suggestion = s3OptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return s3OptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteS3Suggestion(Long id) {
        s3OptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithSuggestions() {
        List<S3OptimizationSuggestion> suggestions = s3OptimizationSuggestionRepository.findAll();
        for (S3OptimizationSuggestion suggestion : suggestions){
            if(suggestion.getStatus() != SuggestionStatus.Pending && Duration.between(suggestion.getCreatedDate().toInstant(), new Date().toInstant()).toDays() > 30){
                s3OptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }

    public boolean hasLifecyclePolicy(String bucketName) {
        StorageBucket bucket = storageBucketRepository.findByName(bucketName);
        AwsAccountCredentials credentials = credentialsService.getCredentialsByAccountId(bucket.getAssociatedAccount());

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey())))
                .withRegion(bucket.getRegion())
                .build();

        GetBucketLifecycleConfigurationRequest request = new GetBucketLifecycleConfigurationRequest(bucketName);
        BucketLifecycleConfiguration configuration = s3Client.getBucketLifecycleConfiguration(request);

        return configuration != null && configuration.getRules().size() > 0;
    }

    public List<S3ObjectSummary> getObjectsListByBucketName(String bucketName){
        StorageBucket bucket = storageBucketRepository.findByName(bucketName);
        AwsAccountCredentials credentials = credentialsService.getCredentialsByAccountId(bucket.getAssociatedAccount());

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey())))
                .withRegion(bucket.getRegion())
                .build();

        ObjectListing objectListing = s3Client.listObjects(bucketName);
        return objectListing.getObjectSummaries();
    }

    public String getOptimalStorageClass(S3ObjectSummary object) {
        Date today = new Date();
        Date lastAccessed = object.getLastModified();
        String storageClass = object.getStorageClass();
        long retentionPeriodInDays = Duration.between(lastAccessed.toInstant(), today.toInstant()).toDays();
        if (storageClass.equals("STANDARD") && retentionPeriodInDays > 30) {
            return "STANDARD_IA";
        } else if (storageClass.equals("STANDARD_IA") && retentionPeriodInDays > 60) {
            return "GLACIER";
        } else if (storageClass.equals("GLACIER") && retentionPeriodInDays > 90) {
            return "DEEP_ARCHIVE";
        } else {
            return storageClass;
        }
    }

    public List<S3ObjectOptimizationSuggestion> generateS3ObjectSuggestions() throws Exception {
        List<StorageBucket> buckets = storageBucketRepository.findAll();
        List<S3ObjectOptimizationSuggestion> suggestions = new ArrayList<>();
        for (StorageBucket bucket : buckets) {
            List<S3ObjectSummary> objects = getObjectsListByBucketName(bucket.getName());
            for (S3ObjectSummary object : objects) {
                String optimalStorageClass = getOptimalStorageClass(object);
                if (!optimalStorageClass.equals(object.getStorageClass())) {
                    S3ObjectOptimizationSuggestion suggestion = S3ObjectOptimizationSuggestion.builder()
                            .bucketName(bucket.getName())
                            .title("Change Storage Class of Object: " + object.getKey())
                            .description("Object " + object.getKey() + " is stored in " + object.getStorageClass() + " storage class. " +
                                    "Consider changing it to " + optimalStorageClass + " to save costs.")
                            .createdDate(new Date())
                            .status(SuggestionStatus.Pending)
                            .associatedAccount(bucket.getAssociatedAccount())
                            .build();
                    suggestions.add(suggestion);
                }
            }
        }

        return s3ObjectOptimizationSuggestionRepository.saveAll(suggestions);
    }

    public List<S3ObjectOptimizationSuggestion> getAllS3ObjectSuggestions() {
        return s3ObjectOptimizationSuggestionRepository.findAll();
    }

    public S3ObjectOptimizationSuggestion getS3ObjectSuggestionById(Long id) {
        return s3ObjectOptimizationSuggestionRepository.findById(id).orElse(null);
    }

    public S3ObjectOptimizationSuggestion updateS3ObjectSuggestionStatus(SuggestionStatus status, Long id) {
        S3ObjectOptimizationSuggestion suggestion = s3ObjectOptimizationSuggestionRepository.findById(id).orElseThrow();
        suggestion.setStatus(status);
        return s3ObjectOptimizationSuggestionRepository.save(suggestion);
    }

    public void deleteS3ObjectSuggestion(Long id) {
        s3ObjectOptimizationSuggestionRepository.deleteById(id);
    }

    public void deleteDealtWithObjectSuggestions() {
        List<S3ObjectOptimizationSuggestion> suggestions = s3ObjectOptimizationSuggestionRepository.findAll();
        for (S3ObjectOptimizationSuggestion suggestion : suggestions) {
            if (suggestion.getStatus() != SuggestionStatus.Pending  && Duration.between(suggestion.getCreatedDate().toInstant(), new Date().toInstant()).toDays() > 30) {
                s3ObjectOptimizationSuggestionRepository.delete(suggestion);
            }
        }
    }


}
