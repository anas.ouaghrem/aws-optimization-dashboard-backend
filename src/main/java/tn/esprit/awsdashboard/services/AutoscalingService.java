package tn.esprit.awsdashboard.services;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.Instance;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AutoscalingService {

    private final UtilityServices utilityServices;

    public List<AutoScalingGroup> getAllAutoScalingGroups() {
        List<AutoScalingGroup> groups = new ArrayList<>();
        for (Pair<AmazonAutoScaling,String> pair : utilityServices.getAutoscalingClientsList()) {
            AmazonAutoScaling client = pair.getLeft();
            String accountId = pair.getRight();

            DescribeAutoScalingGroupsResult result = client.describeAutoScalingGroups();
            groups.addAll(result.getAutoScalingGroups());
        }
        return groups;
    }

    public List<String> checkAutoscalingEnabled(List<String> instanceIds) {
        List<String> instancesWithAutoscaling = new ArrayList<>();

        for (AutoScalingGroup group : getAllAutoScalingGroups()) {
            List<String> instanceIdsInGroup = new ArrayList<>();
            for (Instance instance : group.getInstances()) {
                instanceIdsInGroup.add(instance.getInstanceId());
            }

            for (String id : instanceIds) {
                if (instanceIdsInGroup.contains(id)) {
                    instancesWithAutoscaling.add(id);
                }
            }
        }

        return instancesWithAutoscaling;
    }

    public boolean isInstanceInAutoScalingGroup(String resourceId) {
        List<AutoScalingGroup> autoScalingGroups = getAllAutoScalingGroups();
        for (AutoScalingGroup group : autoScalingGroups) {
            List<Instance> instances = group.getInstances();
            for (Instance instance : instances) {
                String instanceId = instance.getInstanceId();
                if (instanceId.equals(resourceId)) {
                    return true;
                }
            }
        }
        return false;
    }

}
