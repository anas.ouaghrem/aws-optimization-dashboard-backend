package tn.esprit.awsdashboard.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tn.esprit.awsdashboard.entities.AwsAccountCredentials;
import tn.esprit.awsdashboard.repositories.AwsCredentialsRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class CredentialsService {

    private final AwsCredentialsRepository awsCredentialsRepository;

    public AwsAccountCredentials createCredentials(AwsAccountCredentials credentials) {
        credentials.setVerified(false);
        credentials.setDate(new Date());
        try {
            credentials.setAccountId(getAccountId(credentials.getAccessKeyId(), credentials.getSecretAccessKey()));
            credentials.setVerified(true);
            return awsCredentialsRepository.save(credentials);
        } catch (Exception e) {
            throw new RuntimeException("Invalid AWS credentials");
        }
    }

    public List<AwsAccountCredentials> getAllAwsCredentials() {
        return awsCredentialsRepository.findAll();
    }

    public AwsAccountCredentials updateCredentials(Long credentialsId, String name, String accessKeyID, String secretAccessKey){
        AwsAccountCredentials credentials = awsCredentialsRepository.findById(credentialsId).orElseThrow( () -> new RuntimeException("Invalid AWS credentials ID"));
        credentials.setAccountName(name);
        credentials.setAccessKeyId(accessKeyID);
        credentials.setSecretAccessKey(secretAccessKey);
        return awsCredentialsRepository.save(credentials);
    }

    public AwsAccountCredentials getCredentialsById(Long credentialsId){
        return awsCredentialsRepository.findById(credentialsId).orElseThrow( () -> new RuntimeException("Invalid AWS credentials ID"));
    }

    public AwsAccountCredentials getCredentialsByName(String name){
        return awsCredentialsRepository.findAwsAccountCredentialsByAccountName(name);
    }

    public AwsAccountCredentials getCredentialsByAccountId(String accountID){
        return awsCredentialsRepository.findAwsAccountCredentialsByAccountId(accountID);
    }

    public List<AWSCredentials> getAllCredentials(){
        List<AwsAccountCredentials> credentials = awsCredentialsRepository.findAll();
        List<AWSCredentials> result = new ArrayList<>();
        for (AwsAccountCredentials credential : credentials){
            result.add(new BasicAWSCredentials(credential.getAccessKeyId(), credential.getSecretAccessKey()));
        }
        return result;
    }

    public void deleteCredentials(Long credentialsID){
        awsCredentialsRepository.deleteById(credentialsID);
    }

    public AWSCredentials getAwsCredentials(Long awsCredentialsId){
        AwsAccountCredentials credentials = awsCredentialsRepository.findById(awsCredentialsId).orElse(null);
        if (credentials == null) {
            // Credentials not found in the database
            throw new IllegalArgumentException("Invalid AWS credentials ID");
        }
        return new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey());
    }

    // TODO: Verify if this function is still needed

    public boolean testAwsConnection(Long awsCredentialsId) {

        AwsAccountCredentials credentials = awsCredentialsRepository.findById(awsCredentialsId).orElse(null);
        if (credentials == null) {
            throw new IllegalArgumentException("Invalid AWS credentials ID");
        }

        AWSCredentials awsCredentials = new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey());

        AmazonS3Client s3Client = new AmazonS3Client(awsCredentials);
        s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));

        try {
            List<Bucket> buckets = s3Client.listBuckets();
            credentials.setVerified(true);
            awsCredentialsRepository.save(credentials);
            return true;
        } catch (AmazonS3Exception e) {
            return false;
        }
    }

    public  String getAccountId(String accessKey, String secretKey) {
        String accountId = null;

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.DEFAULT_REGION)
                .build();

        GetCallerIdentityRequest request = new GetCallerIdentityRequest();

        GetCallerIdentityResult result = sts.getCallerIdentity(request);

        accountId = result.getAccount();

        return accountId;
    }


}
