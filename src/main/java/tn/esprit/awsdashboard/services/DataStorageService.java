package tn.esprit.awsdashboard.services;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tn.esprit.awsdashboard.DTO.*;
import tn.esprit.awsdashboard.entities.*;
import tn.esprit.awsdashboard.repositories.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DataStorageService {

    private final Ec2Service ec2Service;
    private final EC2InstanceRepository ec2InstanceRepository;
    private final EBSVolumeRepository ebsVolumeRepository;
    private final EIPAddressRepository eipAddressRepository;
    private final S3Service s3Service;
    private final StorageBucketRepository storageBucketRepository;
    private final RdsService rdsService;
    private final RDSInstanceRepository rdsInstanceRepository;
    private final CredentialsService credentialsService;

    @Scheduled(fixedDelay = 3600000L)
    public void storeEc2Instances() throws Exception {
        ec2InstanceRepository.deleteAll();
        System.out.println("Storing EC2 Instances ......");
        List<Ec2DTO> ec2DTOList = ec2Service.getAllEC2InstancesWithAccountId();
        List<EC2Instance> ec2InstanceList = new ArrayList<>();

        for (Ec2DTO ec2DTO : ec2DTOList) {
            EC2Instance ec2Instance = EC2Instance.builder()
                    .instanceId(ec2DTO.getInstanceId())
                    .instanceType(ec2DTO.getInstanceType())
                    .associatedAccount(ec2DTO.getAssociatedAccount())
                    .region(ec2DTO.getRegion())
                    .ownerEmail(ec2DTO.getTags().get("OwnerEmail"))
                    .state(ec2DTO.getState())
                    .platform(ec2DTO.getPlatform())
                    .environmentType(ec2DTO.getTags().get("EnvironmentType"))
                    .operationHours(ec2DTO.getTags().get("OperationHours"))
                    .productId(ec2DTO.getTags().get("ProductId"))
                    .ClientName(ec2DTO.getTags().get("ClientName"))
                    .publicIp(ec2DTO.getPublicIp())
                    .privateIp(ec2DTO.getPrivateIp())
                    .build();
            ec2InstanceList.add(ec2Instance);
        }
        ec2InstanceRepository.saveAll(ec2InstanceList);
        System.out.println("EC2 Instances stored successfully");
    }

    @Scheduled(fixedDelay = 3600000L)
    public void storeEbsVolumes() throws Exception {
        System.out.println("Storing EBS Volumes ......");
        List<EbsDTO> ebsDTOList = ec2Service.getAllEbsVolumesWithAccountId();
        List<EBSVolume> ebsVolumes = new ArrayList<>();

        for (EbsDTO ec2DTO : ebsDTOList) {
            ebsVolumeRepository.deleteAll();
            EBSVolume ebsVolume = EBSVolume.builder()
                    .volumeId(ec2DTO.getVolumeId())
                    .associatedAccount(ec2DTO.getAssociatedAccount())
                    .volumeType(ec2DTO.getVolumeType())
                    .creationTime(ec2DTO.getCreationTime())
                    .availabilityZone(ec2DTO.getRegion())
                    .size(ec2DTO.getSize())
                    .state(ec2DTO.getState())
                    .instanceId(ec2DTO.getInstanceId())
                    .productId(ec2DTO.getTags().get("ProductId"))
                    .ownerEmail(ec2DTO.getTags().get("OwnerEmail"))
                    .operationHours(ec2DTO.getTags().get("OperationHours"))
                    .environmentType(ec2DTO.getTags().get("EnvironmentType"))
                    .build();
            ebsVolumes.add(ebsVolume);
        }
        ebsVolumeRepository.saveAll(ebsVolumes);
        System.out.println("EBS Volumes stored successfully");
    }

    @Scheduled(fixedDelay = 3600000L)
    public void storeEipAddresses(){
        eipAddressRepository.deleteAll();
        System.out.println("Storing EIP Addresses ......");
        List<EipDTO> eipDTOList = ec2Service.getAllElasticIPsWithAccountId();
        List< EIPAddress> eipAddresses = new ArrayList<>();

        for (EipDTO eipDTO : eipDTOList) {
            EIPAddress eipAddress = EIPAddress.builder()
                    .associatedAccount(eipDTO.getAssociatedAccount())
                    .publicIp(eipDTO.getPublicIp())
                    .allocationId(eipDTO.getAllocationId())
                    .associationId(eipDTO.getAssociationId())
                    .privateIp(eipDTO.getPrivateIp())
                    .instanceId(eipDTO.getInstanceId())
                    .build();
            eipAddresses.add(eipAddress);
        }
        eipAddressRepository.saveAll(eipAddresses);
        System.out.println("EIP Addresses stored successfully");
    }

    @Scheduled(fixedDelay = 3600000L)
    public void storeRdsInstances(){
        rdsInstanceRepository.deleteAll();
        System.out.println("Storing RDS Instances ......");
        List<RdsDTO> rdsDTOList = rdsService.getAllRdsInstancesWithAccountId();
        List<RDSInstance> rdsInstances = new ArrayList<>();

        for (RdsDTO rdsDTO : rdsDTOList) {
            RDSInstance rdsInstance = RDSInstance.builder()
                    .associatedAccount(rdsDTO.getAssociatedAccount())
                    .dbInstanceIdentifier(rdsDTO.getDbInstanceIdentifier())
                    .dbInstanceClass(rdsDTO.getDbInstanceClass())
                    .engine(rdsDTO.getEngine())
                    .engineVersion(rdsDTO.getEngineVersion())
                    .status(rdsDTO.getStatus())
                    .allocatedStorage(rdsDTO.getAllocatedStorage())
                    .ownerEmail(rdsDTO.getTags().get("OwnerEmail"))
                    .clientName(rdsDTO.getTags().get("ClientName"))
                    .productId(rdsDTO.getTags().get("ProductId"))
                    .operationHours(rdsDTO.getTags().get("OperationHours"))
                    .endpointPort(rdsDTO.getEndpointPort())
                    .endpointAddress(rdsDTO.getEndpointAddress())
                    .creationDate(rdsDTO.getCreationDate())
                    .region(rdsDTO.getRegion())
                    .build();
            rdsInstances.add(rdsInstance);
        }
        rdsInstanceRepository.saveAll(rdsInstances);
        System.out.println("RDS Instances stored successfully");
    }

    @Scheduled(fixedDelay = 3600000L)
    public void storeS3Buckets() throws Exception {
        storageBucketRepository.deleteAll();
        System.out.println("Storing S3 Buckets ......");
        List<StorageBucket> storageBuckets = s3Service.s3testAllRegionsStream().stream()
                .map(s3BucketDTO -> StorageBucket.builder()
                        .associatedAccount(s3BucketDTO.getAssociatedAccount())
                        .name(s3BucketDTO.getName())
                        .creationDate(s3BucketDTO.getCreationDate())
                        .purpose(s3BucketDTO.getTags().get("Purpose"))
                        .ownerEmail(s3BucketDTO.getTags().get("OwnerEmail"))
                        .objectCount(s3BucketDTO.getObjectCount())
                        .size(s3BucketDTO.getSize())
                        .owner(s3BucketDTO.getOwnerName())
                        .region(s3BucketDTO.getRegion())
                        .build())
                .collect(Collectors.toList());
        storageBucketRepository.saveAll(storageBuckets);
        System.out.println("S3 Buckets stored successfully");
    }

}
