package tn.esprit.awsdashboard.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import com.amazonaws.services.support.AWSSupport;
import com.amazonaws.services.support.AWSSupportClientBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tn.esprit.awsdashboard.entities.AwsAccountCredentials;
import tn.esprit.awsdashboard.repositories.AwsCredentialsRepository;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class UtilityServices {

    private final CredentialsService awsService;
    private final AwsCredentialsRepository awsCredentialsRepository;
    private static final Regions[] awsRegions = {
            Regions.AP_SOUTH_1,
            Regions.EU_NORTH_1,
            Regions.EU_WEST_3,
            Regions.EU_WEST_2,
            Regions.EU_WEST_1,
            Regions.AP_NORTHEAST_3,
            Regions.AP_NORTHEAST_2,
            Regions.AP_NORTHEAST_1,
            Regions.CA_CENTRAL_1,
            Regions.SA_EAST_1,
            Regions.AP_SOUTHEAST_1,
            Regions.AP_SOUTHEAST_2,
            Regions.EU_CENTRAL_1,
            Regions.US_EAST_1,
            Regions.US_EAST_2,
            Regions.US_WEST_1,
            Regions.US_WEST_2,

    };

    public List<Pair<AmazonEC2, String>> getEc2ClientsListWithAccountId() {
        List<Pair<AmazonEC2, String>> ec2ClientsListWithAccountId = new ArrayList<>();
        for (AWSCredentials credentials : this.awsService.getAllCredentials()){
            String accountId = getAccountId(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey());
            for (Regions region : awsRegions) {
                AmazonEC2 client = AmazonEC2ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(),credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build();
                ec2ClientsListWithAccountId.add(new ImmutablePair<>(client, accountId));
            }
        }
        return ec2ClientsListWithAccountId;
    }

    @Cacheable("rds-clients")
    public List<Pair<AmazonRDS, String>> getRDSClientsListWithAccountId () {
        List<Pair<AmazonRDS, String>> rdsClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()){
            String accountId = getAccountId(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey());
            for (Regions region : awsRegions) {
                AmazonRDS client = AmazonRDSClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(),credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build();
                rdsClientsList.add(new ImmutablePair<>(client, accountId));
            }
        }
        return rdsClientsList;
    }

    @Cacheable("s3-clients")
    public List<Pair<AmazonS3, String>> getS3ClientsList() {
        List<Pair<AmazonS3, String>> s3ClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()) {
            String accountId = getAccountId(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey());
            for (Regions region : awsRegions) {
                AmazonS3 client = AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .withForceGlobalBucketAccessEnabled(true)
                        .build();
                s3ClientsList.add(new ImmutablePair<>(client, accountId));
            }
        }
        return s3ClientsList;
    }

    @Cacheable("trusted-advisor-clients")
    public List<Pair<AWSSupport,String>> getTrustedAdvisorClientsList() {
        List<Pair<AWSSupport, String>> trustedAdvisorClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()) {
            String accountId = getAccountId(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey());
            for (Regions region : awsRegions) {
                AWSSupport client = AWSSupportClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build();
                trustedAdvisorClientsList.add(new ImmutablePair<>(client, accountId));
            }
        }
        return trustedAdvisorClientsList;
    }

    @Cacheable("asg-clients")
    public List<Pair<AmazonAutoScaling,String>> getAutoscalingClientsList(){
        List<Pair<AmazonAutoScaling, String>> autoscalingClientsList = new ArrayList<>();

        for (AWSCredentials credentials : this.awsService.getAllCredentials()) {
            String accountId = getAccountId(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey());
            for (Regions region : awsRegions) {
                AmazonAutoScaling client = AmazonAutoScalingClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAWSAccessKeyId(), credentials.getAWSSecretKey())))
                        .withRegion(region)
                        .build();
                autoscalingClientsList.add(new ImmutablePair<>(client, accountId));
            }
        }
        return autoscalingClientsList;
    }

    public String getAwsAccountName(String awsAccountKeyID) {

        AwsAccountCredentials credentials = this.awsCredentialsRepository.findAwsAccountCredentialsByAccessKeyId(awsAccountKeyID);

        AmazonIdentityManagement iam = AmazonIdentityManagementClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(credentials.getAccessKeyId(), credentials.getSecretAccessKey())))
                .build();

        String accountAlias = iam.listAccountAliases().getAccountAliases().get(0);
        if (accountAlias != null && !accountAlias.isEmpty()) {
            return accountAlias;
        } else {
            return iam.listUsers().getUsers().get(0).getArn().split(":")[4];
        }
    }

    public  String getAccountId(String accessKey, String secretKey) {
        String accountId = null;

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.DEFAULT_REGION)
                .build();

        GetCallerIdentityRequest request = new GetCallerIdentityRequest();

        GetCallerIdentityResult result = sts.getCallerIdentity(request);

        accountId = result.getAccount();

        return accountId;
    }

}
